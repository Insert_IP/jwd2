package edu.student.task2.entity;

public class Waypoint {
    private int waypointNum;

    public Waypoint() {
    }
    
    public Waypoint(int waypointNum) {
        this.waypointNum = waypointNum;
    }

    public int getWaypointNum() {
        return waypointNum;
    }

    public void setWaypointNum(int waypointNum) {
        this.waypointNum = waypointNum;
    }
}
