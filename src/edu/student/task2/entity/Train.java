package edu.student.task2.entity;

import edu.student.task2.action.TrafficControllOffice;
import edu.student.task2.exception.SlotException;
import edu.student.task2.generator.PauseGenerator;
import org.apache.log4j.Logger;

public class Train extends Thread {
    
    private final Logger LOG = Logger.getLogger(Train.class);
    private int trainId;
    private int passengers;
    private TrafficControllOffice<Waypoint> ctl;

    public Train() {
    }

    public Train(int trainId, int passengers, TrafficControllOffice<Waypoint> ctl) {
        this.trainId = trainId;
        this.passengers = passengers;
        this.ctl = ctl;
    }
    
    public int getTrainId() {
        return trainId;
    }

    public void setTrainId(int trainId) {
        this.trainId = trainId;
    }

    public int getPassengers() {
        return passengers;
    }

    public void setPassengers(int passengers) {
        this.passengers = passengers;
    }
    
    @Override
    public void run() {
        Waypoint w = null;
        try{
            w = ctl.incoming();            
            LOG.info(trainId + " arrived on waypoint #" + w.getWaypointNum() + " with " + this.getPassengers() + " passengers");
            ctl.passTrasfer(this);
            
            try {
                sleep(PauseGenerator.generatePause());
            } catch (InterruptedException ex) {
                LOG.error(ex);
            }
        } catch(SlotException e){
            LOG.error("No slots available");
        } finally {
            LOG.info(trainId + " departed" + " with " + this.getPassengers() + " passengers");
            ctl.departure(w);
            
        }
    }
}
