package edu.student.task2.action;

import edu.student.task2.creator.Creator;
import edu.student.task2.entity.Train;
import edu.student.task2.exception.SlotException;
import java.util.LinkedList;
import java.util.Random;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.log4j.Logger;

public class TrafficControllOffice<T> {

    private final Logger LOG = Logger.getLogger(TrafficControllOffice.class);
    private final int MAX_WAYPOINTS = 2;
    
    private static TrafficControllOffice instance = null;
    private static boolean created;
    private static ReentrantLock lock = new ReentrantLock();
    private static int onPlatform = 0;
    
    private Semaphore semaphore = new Semaphore(MAX_WAYPOINTS, true);
    private LinkedList<T> platformSlots = null;

    private Random rand = new Random();

    private TrafficControllOffice(LinkedList<T> wayPoints) {
        platformSlots = new LinkedList<>();
        platformSlots.addAll(wayPoints);
    }
    
    public static int getOnPlatform() {
        return onPlatform;
    }
    
    public static void setOnPlatform(int quantity){
        onPlatform = quantity;
    }
    
    public static TrafficControllOffice getInstance(){
        if(!created){
            try{
                lock.lock();
                if(instance == null){
                    instance = new TrafficControllOffice(Creator.generatePlatforms());
                    created = true;
                }
            } finally {
                lock.unlock();
            }
        }
        return instance;
    }

    public T incoming() throws SlotException {
        T con = null;
        try {
            semaphore.acquire();
            con = platformSlots.poll();
            
        } catch (InterruptedException e) {
            throw new SlotException("There Is No Slots Available");    
        }
        return con;
    }

    public void departure(T con) {
        platformSlots.add(con);
        semaphore.release();
    }

    public void passTrasfer(Train t) {
        lock.lock();
        try {
            passengersDeploy(t);
            passengersLanding(t);
        } finally {
            lock.unlock();
        }
    }
    
    private void passengersDeploy(Train t) {
        int delta = 1 + rand.nextInt(t.getPassengers());
        int offPlatform = 1 + rand.nextInt(delta);
        
        t.setPassengers(t.getPassengers() - delta);
        LOG.info(delta + " gone, " + t.getPassengers() + " left");

        TrafficControllOffice.setOnPlatform(TrafficControllOffice.getOnPlatform() + delta - offPlatform);
        LOG.info(offPlatform + " from platform, " + TrafficControllOffice.getOnPlatform() + " left on platform");
    }

    private void passengersLanding(Train t) {
        int delta = 1 + rand.nextInt(TrafficControllOffice.getOnPlatform());
        
        t.setPassengers(t.getPassengers() + delta);
        LOG.info(delta + " on train " + t.getPassengers() + " passengers");
        
        TrafficControllOffice.setOnPlatform(TrafficControllOffice.getOnPlatform() - delta);
        LOG.info(TrafficControllOffice.getOnPlatform() + " left on platform");
    }

}
