package edu.student.task2.creator;

import edu.student.task2.action.TrafficControllOffice;
import edu.student.task2.entity.Train;
import edu.student.task2.entity.Waypoint;
import java.util.LinkedList;
import java.util.Random;

public class Creator {
    
    private static final int MAX_TRAINS = 10;
    private static final int MAX_PASSENGERS = 400;
    
    private static Random rand = new Random();
    
    public static LinkedList<Waypoint> generatePlatforms(){
        LinkedList<Waypoint> platformList = new LinkedList<>();
        platformList.add(new Waypoint(1));
        platformList.add(new Waypoint(2));
        return platformList;
    }
    
    public static void generateTraffic(){
        TrafficControllOffice<Waypoint> radioSet = TrafficControllOffice.getInstance();
        for(int i = 0; i < MAX_TRAINS; i++){
            new Train(i, rand.nextInt(MAX_PASSENGERS), radioSet).start();
        }
    }
}
