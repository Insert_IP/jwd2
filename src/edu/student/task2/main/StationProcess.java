package edu.student.task2.main;

import edu.student.task2.creator.Creator;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;

public class StationProcess {

    private static final String CONFIG_PATH = "config/log4j.xml";

    static {
        new DOMConfigurator().doConfigure(CONFIG_PATH, LogManager.getLoggerRepository());
    }

    public static void main(String[] args) {
        Creator.generateTraffic();
    }
}
