package edu.student.task2.generator;

import java.util.Random;

public class PauseGenerator {
    private static final int MAX_PAUSE = 1000;
    private static final int MIN_PAUSE = 400;
    private static Random rand = new Random();
    
    public static int generatePause(){
        return rand.nextInt(MAX_PAUSE);
    }
}
