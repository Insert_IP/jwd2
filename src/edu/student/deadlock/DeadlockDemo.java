package edu.student.deadlock;

public class DeadlockDemo {

    public static void main(String[] args) {
        String a = "a";
        String b = "b";
        new Thread() {
            @Override
            public void run() {
                synchronized (a) {
                    System.out.println("Calling " + a);
                    synchronized (b) {
                        System.out.println(a + " " + b);
                    }
                }
            }
        }.start();

        new Thread() {
            @Override
            public void run() {
                synchronized (b) {
                    System.out.println("Calling " + b);
                    synchronized (a) {
                        System.out.println(a + " " + b);
                    }
                }
            }
        }.start();
    }
}
